﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_exercicePoo
{
    internal class Cercle : Forme
    {

        public double Rayon { get; set; } = 1.0;
        public Cercle(double rayon,  Couleurs couleur) : base(couleur)
        {
            Rayon = rayon;
        }

        public override double CalculSurface()
        {
           return  Rayon*Rayon*Math.PI;
        }
    }
}
