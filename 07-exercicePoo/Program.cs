﻿using System;

namespace _07_exercicePoo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Terrain terrain = new Terrain();
            terrain.Ajouter(new Cercle(1.0, Couleurs.VERT));
            terrain.Ajouter(new Cercle(1.0, Couleurs.VERT));
            terrain.Ajouter(new Rectangle(2.0, 1.0, Couleurs.BLEU));
            terrain.Ajouter(new Rectangle(2.0, 1.0, Couleurs.BLEU));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ROUGE));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ROUGE));
            terrain.Ajouter(new TriangleRectangle(1.0, 1.0, Couleurs.ORANGE));
            terrain.Ajouter(new Rectangle(2.0, 1.0, Couleurs.ORANGE));

            Console.WriteLine($"Surface total= {terrain.Surface()}");
            Console.WriteLine($"Surface Vert= {terrain.Surface(Couleurs.VERT)}");
            Console.WriteLine($"Surface Bleu= {terrain.Surface(Couleurs.BLEU)}");
            Console.WriteLine($"Surface Rouge= {terrain.Surface(Couleurs.ROUGE)}");
            Console.WriteLine($"Surface Orange= {terrain.Surface(Couleurs.ORANGE)}");

            Console.ReadKey();

        }
    }
}
