﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _07_exercicePoo
{
    internal class Terrain
    {
        Forme[] _formes = new Forme[20];
        int _cptForme;

        public void Ajouter(Forme f)
        {
            if (_cptForme < _formes.Length)
            {
                _formes[_cptForme] = f;
                _cptForme++;
            }
        }

        public double Surface()
        {
            double somme = 0.0;
            for (int i = 0; i < _cptForme; i++)
            {
                somme += _formes[i].CalculSurface(); ;
            }
            return somme;
        }

        public double Surface(Couleurs couleur)
        {
            double somme = 0.0;
            for (int i = 0; i < _cptForme; i++)
            {
                if (_formes[i].Couleur == couleur)
                {
                    somme += _formes[i].CalculSurface();
                }
            }
            return somme;


        }
    }
}

