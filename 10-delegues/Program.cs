﻿using System;

namespace _10_delegues
{
    internal class Program
    {
        // Prototype
        public delegate int Operation(int a, int b);
        public delegate bool Comparaison(int a, int b);

        // C# 1.0
        // ---------------------
        public static int Ajouter(int n1, int n2)
        {
            return n1 + n2;
        }

        public static int Multiplier(int a, int b)
        {
            return a * b;
        }
        // ---------------------

        static void Main(string[] args)
        {
            // C #1
            Operation add = new Operation(Ajouter);
            int res = Calcul(2, 4, add);
            Console.WriteLine(res);
            res = Calcul(2, 4, new Operation(Multiplier));
            Console.WriteLine(res);

            // C #2
            Operation add2 = delegate (int a, int b) { return a + b; };
            res = Calcul(2, 4, add);
            Console.WriteLine(res);
            res = Calcul(2, 4, delegate (int a, int b) { return a * b; });
            Console.WriteLine(res);

            // C #3
            // res = Calcul(2, 4, (int a, int b) => { return a + b; });
            res = Calcul(2, 4, (a, b) => a + b);
            Console.WriteLine(res);
            res = Calcul(2, 4, (x, y) => x * y);
            Console.WriteLine(res);

            // Exercice Delegate
            int[] tab = { 3, 7, 1, 8, 9, -1, 3 };
            SortTab(tab, (a, b) => a > b);
            foreach (var e in tab)
            {
                Console.WriteLine(e);
            }

            SortTab(tab, (a, b) => a < b);
            foreach (var e in tab)
            {
                Console.WriteLine(e);
            }

            // Délégués Func
            double d = 10.0;
            Func<double, double> multi = x => x * d;
            Console.WriteLine(multi(2.0));

            Console.WriteLine(CalculF(4, 6, (x, y) => x - y));
            Console.ReadKey();
        }

        public static int Calcul(int a, int b, Operation op)
        {
            return op(a, b);
        }

        public static int CalculF(int a, int b, Func<int, int, int> op)
        {
            return op(a, b);
        }

        // static void SortTab(int[] tab, Comparaison cmp) 
        static void SortTab(int[] tab, Func<int, int, bool> cmp)
        {
            bool notDone = true;
            int end = tab.Length - 1;
            while (notDone)
            {
                notDone = false;
                for (int i = 0; i < end; i++)
                {
                    if (cmp(tab[i], tab[i + 1]))
                    {
                        int tmp = tab[i];
                        tab[i] = tab[i + 1];
                        tab[i + 1] = tmp;
                        notDone = true;
                    }
                }
                end--;
            }
        }
    }
}
