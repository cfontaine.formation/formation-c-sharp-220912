﻿namespace _12_bcl
{
    internal class Box<T> //where T: Animal
    {
        public T A { get; set; }

        public T B { get; set; }

        public Box(T a, T b)
        {
            A = a;
            B = b;
        }

        public override string ToString()
        {
            return $" {A}  {B}";
        }
    }
}
