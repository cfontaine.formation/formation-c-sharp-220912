﻿using System;

namespace _12_bcl
{
    internal class TestMethodeGenerique
    {

        public static void MethodeGenerique<T>(T a)
        {
            Console.WriteLine(a);
        }
    }
}
