﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _02_instructions
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Condition
            // Condition if
            int val = Convert.ToInt32(Console.ReadLine());
            if (val > 100)
            {
                Console.WriteLine("La valeur saisie est supérieur à 100");
            }
            else if (val == 100)
            {
                Console.WriteLine("La valeur saisie est égale à 100");
            }
            else
            {
                Console.WriteLine("La valeur saisie est inférieure à 100");
            }


            // exercice: trie de 3 valeurs
            // saisir 3 nombres et afficher ces nombres triés dans l'ordre croissant sous la forme 1.5 < 10.5 < 50.7
            Console.WriteLine("trie: saisir 3 nombres réels");
            double valeur1 = Convert.ToDouble(Console.ReadLine());
            double valeur2 = Convert.ToDouble(Console.ReadLine());
            double valeur3 = Convert.ToDouble(Console.ReadLine());
            if (valeur1 > valeur2)
            {
                double tmp = valeur1;
                valeur1 = valeur2;
                valeur2 = tmp;
            }
            if (valeur3 < valeur1)
            {
                Console.WriteLine($"{valeur3}<{valeur1}<{valeur2}");
            }
            else if (valeur3 > valeur2)
            {
                Console.WriteLine($"{valeur1}<{valeur2}<{valeur3}");
            }
            else
            {
                Console.WriteLine($"{valeur1}<{valeur3}<{valeur2}");
            }

            //// Exercice: Intervalle
            // Saisir un nombre et dire s'il fait parti de l'intervalle - 4(exclu) et 7(inclu)
            Console.WriteLine("Intervalle: Saisir un entier");
            int v = Convert.ToInt32(Console.ReadLine());
            if (v > -4 && v <= 7)
            {
                Console.WriteLine($"La valeur {v} fait partie de l'interval");
            }

            // Condition switch
            const int J = 6;

            int jours = Convert.ToInt32(Console.ReadLine());
            switch (jours)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case J:
                case J + 1:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Exercice: Calculatrice
            // Faire un programme calculatrice
            // Saisir dans la console
            // - un nombre à virgule flottante v1
            // - une chaîne de caractère opérateur qui a pour valeur valide:  + - * /
            // - un nombre à virgule flottante v2
            // Afficher:
            // - Le résultat de l’opération
            // - Un message d’erreur si l’opérateur est incorrect
            // - Un message d’erreur si l’on fait une division par 0

            double v1 = Convert.ToDouble(Console.ReadLine());
            string op = Console.ReadLine();
            double v2 = Convert.ToDouble(Console.ReadLine());

            switch (op)
            {
                case "+":
                    Console.WriteLine($"{v1} + {v2} = {v1 + v2}");
                    break;
                case "-":
                    Console.WriteLine($"{v1} - {v2} = {v1 - v2}");
                    break;
                case "*":
                    Console.WriteLine($"{v1} * {v2} = {v1 * v2}");
                    break;
                case "/":
                    if (v2 == 0.0)
                    {
                        Console.WriteLine("Division par 0");
                    }
                    else
                    {
                        Console.WriteLine($"{v1} + {v2} = {v1 / v2}");
                    }

                    break;
                default:
                    Console.WriteLine($"L'opératuer {op} n'est pas valide");
                    break;

            }

            // Clause when (C# 7.0) => Ajouter une condition supplémentaire sur un case
            int jo = Convert.ToInt32(Console.ReadLine());
            switch (jo)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    break;
                case var j1 when j1 == 6 || j1 == 7: // <== clause when
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }

            // Opérateur ternaire
            double vd1 = double.Parse(Console.ReadLine());
            double vAbs = vd1 < 0 ? -vd1 : vd1;
            Console.WriteLine($"|{vd1}|= {vAbs}");
            #endregion

            #region boucle
            int j = 0;
            while (j < 10)
            {
                Console.WriteLine(j);
                j++;
            }

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }

            #endregion

            #region Instruction de saut
            // break
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    break;   // break => termine la boucle
                }
                Console.WriteLine(i);
            }

            // break
            for (int i = 0; i < 10; i++)
            {
                if (i == 3)
                {
                    continue;   // continue => on passe à l'itération suivante
                }
                Console.WriteLine(i);
            }

            // goto pour quitter des boucles imbriquées
            for (int i = 0; i < 10; i++)
            {
                for (int k = 0; k < 5; k++)
                {
                    if (k == 2)
                    {
                        goto EXIT_LOOP;
                    }
                }
            }
            EXIT_LOOP:

            // goto avec un switch
            int jour = Convert.ToInt32(Console.ReadLine());
            switch (jour)
            {
                case 1:
                    Console.WriteLine("Lundi");
                    //   goto case 6;
                    goto default;
                case 6:
                case 7:
                    Console.WriteLine("Week end !");
                    break;
                default:
                    Console.WriteLine("Un autre jour");
                    break;
            }
            #endregion


            #region Eercice boucle
            //Table de multiplication
            //Faire un programme qui affiche la table de multiplication pour un nombre entre 1 et 9

            //    1 X 4 = 4
            //    2 X 4 = 8
            //    …
            //    9 x 4 = 36

            //Si le nombre passé en paramètre est en dehors de l’intervalle 1 à 9 on arrête sinon on redemande une nouvelle valeur

            for (; ; )
            {
                int m = int.Parse(Console.ReadLine());
                if (m < 1 || m > 9)
                {
                    break;
                }
                for (int i = 1; i < 10; i++)
                {
                    Console.WriteLine($"{i} * {m} = {i * m}");
                }
            }

            // Quadrillage 
            // Créer un quadrillage dynamiquement on saisit le nombre de colonne et le nombre de ligne
            //    ex: pour 2 3
            //    [ ][ ]
            //    [ ][ ]
            //    [ ][ ]
            Console.WriteLine("Quadrillage");
            Console.Write("Saisir le nombre de colonne: ");
            int col = int.Parse(Console.ReadLine());
            Console.Write("Saisir le nombre de ligne: ");
            int row = int.Parse(Console.ReadLine());
            for (int l = 0; l < row; l++)
            {
                for (int c = 0; c < col; c++)
                {
                    Console.Write("[ ] ");
                }
                Console.WriteLine();
            }
            #endregion
            Console.ReadKey();
        }
    }
}
