﻿using System;



namespace _00_helloworld
{
    /// <summary>
    ///  La classe Program
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// Le point d'entrée du programme
        /// </summary>
        /// <param name="args">paramètres ligne de commande</param>
        static void Main(string[] args)
        {
            /*
             * Commentaires
             * sur 
             * plusieurs lignes*/
            Console.WriteLine("Hello world!"); // commentaires fin de ligne
            Console.ReadKey();
        }
    }
}
