﻿using System;
using System.Text;

namespace _04_methodes
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Appel de methode 
            double res = Multiplier(1.2, 3.4);
            Console.WriteLine(res);

            // Appel de methode (sans type retour)
            AfficherValeur(3);

            // Exercice Maximum
            Console.WriteLine("Maximum: entrer 2 réels");

            int v1 = int.Parse(Console.ReadLine());
            int v2 = int.Parse(Console.ReadLine());
            Console.WriteLine(Maximum(v1, v2));

            // Exercice Paire
            Console.WriteLine(Even(3));
            Console.WriteLine(Even(6));


            // Passage par valeur (par défaut en C#)
            // C'est une copie de la valeur du paramètre qui est transmise à la méthode
            int v = 12;
            TestParamValeur(v);
            Console.WriteLine(v);


            // Passage de paramètre par référence
            //La valeur de la variable passée en paramètre est modifiée, si elle est modifiée dans la méthode
            TestParamReference(ref v);
            Console.WriteLine(v);

            // Passage de paramètre en sortie
            // Un paramètre out ne sert à la méthode qu'à retourner une valeur
            // L'argument transmis doit référencer une variable ou un élément de tableau
            // La variable n'a pas besoin d'être initialisée

            // int o1;
            // int o2;
            // On peut déclarer les variables de retours dans les arguments pendant l'appel de la méthode
            TestParamSortie(out int o1, out int o2);
            Console.WriteLine(o1 + " " + o2);

            // On peut ignorer un paramètre out en le nommant _
            TestParamSortie(out _, out v);
            Console.WriteLine(v);

            // Paramètre optionnel
            TestParamOptionnel(3, false, "bonjour");
            TestParamOptionnel(5, false);
            TestParamOptionnel(6);

            // Paramètres nommés
            TestParamOptionnel(7, s: "bonjour");
            TestParamOptionnel(b: false, s: "world", i: 8);

            // Nombre de paramètres variable
            Console.WriteLine(Moyenne(10));
            Console.WriteLine(Moyenne(10, 8));
            Console.WriteLine(Moyenne(10, 8, 6, 1, 5, 9));
            // Passage d'un tableau en paramètre
            int[] notes = { 8, 6, 1, 5, 9 };
            Console.WriteLine(Moyenne(10, notes));

            StringBuilder sbu = new StringBuilder("hello");
            TestParamValeurObjet(sbu);
            Console.WriteLine(sbu);
            TestParamValeurObjModifEtat(sbu);
            Console.WriteLine(sbu);

            // Exercice Echange
            int va = 3;
            int vb = 5;
            Console.WriteLine($"{va} {vb}");
            Swap(ref va, ref vb);
            Console.WriteLine($"{va} {vb}");

            // Exercice estPresent
            Console.WriteLine(EstPresent('o', 'e', 'r', 'i', 'o', 'y'));
            Console.WriteLine(EstPresent('w', 'e', 'r', 'i', 'o', 'y'));

            // Exercice Tableau
            menu();


            // Surcharge de méthode
            // Correspondance exacte des type des paramètres
            Console.WriteLine(Somme(1, 2));
            Console.WriteLine(Somme(1, 3.0));
            Console.WriteLine(Somme(1.5, 3.0));
            Console.WriteLine(Somme("az", "erty"));

            // Pas de correspondance exacte => convertion automatique
            Console.WriteLine(Somme(1L, 23));   // => appel de la méthode avec 2 double en paramètres
            Console.WriteLine(Somme(23, 2L));   // => appel de la méthode avec 1 entier et un double en paramètre
            Console.WriteLine(Somme('a', 4));    // => appel de la méthode avec 2 entiers en paramètres

            // Pas de conversion possible => Erreu
            // Console.WriteLine(Somme("a", 4));
            // Console.WriteLine(Somme(45.4M, 45.4M));

            // Paramètre de la méthode Main
            // En lignes de commandes: 05-methode.exe Azerty 12234 "aze rty"
            // Dans visual studio: propriétés du projet-> onglet Déboguer-> options de démarrage -> Arguments de la ligne de commande
            foreach (var a in args)
            {
                Console.WriteLine(a);
            }

            // Méthode récursive
            double fact3 = Factorial(3);
            Console.WriteLine(fact3);

            // Méthode locale
            AfficherSquare(4.0);
            Console.ReadKey();
        }

        // Déclaration
        static double Multiplier(double v1, double v2)
        {
            return v1 * v2; // L'instruction return
                            // - Interrompt l'exécution de la méthode
                            // - Retourne la valeur de l'expression à droite
        }

        static void AfficherValeur(int a)   // void => pas de valeur retourné
        {
            Console.WriteLine(a);
            // avec void => return; ou return peut être omis ;
        }

        #region Exercice Maximum
        // Ecrire une méthode Maximum qui prends en paramètre 2 nombres et elle retourne le maximum
        static int Maximum(int a, int b)
        {
            //if (a > b)
            //{
            //    return a;
            //}
            //return b;
            return a > b ? a : b;
        }
        #endregion

        #region Exercice Paire
        // Écrire une méthode Paire qui prend un entier en paramètre un entier
        // Elle retourne vrai, si il est paire

        static bool Even(int value)
        {
            //if (value % 2 == 0)
            //{
            //    return true;
            //}
            //return false;
            return value % 2 == 0;
            //    return (value & 0b1) == 0;
        }
        #endregion

        #region passage de paramètre

        // Passage par valeur
        static void TestParamValeur(int a)
        {
            Console.WriteLine(a);
            a = 42; // La modification de la valeur du paramètre a n'a pas d'influence en dehors de la méthode
            Console.WriteLine(a);
        }

        // Passage par référence => ref
        static void TestParamReference(ref int a)
        {
            Console.WriteLine(a);
            a = 42;
            Console.WriteLine(a);
        }

        // Passage de paramètre en sortie => out
        static void TestParamSortie(out int a, out int b)
        {
            //int i = a * 2;        // erreur, on ne peut pas accéder en entrée à un paramètre out 
            //Console.WriteLine(a);
            a = 42;
            int i = a * 2;
            Console.WriteLine(a);
            b = 5;               // La méthode doit obligatoirement affecter une valeur aux paramètres out
        }

        // On peut donner une valeur par défaut aux arguments passés par valeur
        static void TestParamOptionnel(int i, bool b = true, string s = "Hello")
        {
            Console.WriteLine($" i={i} b={b} s={s}");
        }

        // Nombre d'arguments variable => params
        static double Moyenne(int n, params int[] notes)
        {
            double somme = n;
            foreach (var elm in notes)
            {
                somme += elm;
            }
            return somme / (notes.Length + 1);
        }

        // Passage par valeur d'un objet
        // La référence de l'objet passé en paramètre est copiée dans la référence sb  
        static void TestParamValeurObjet(StringBuilder sb)
        {
            Console.WriteLine(sb);
            sb = new StringBuilder("autre chaine");    // On crée un nouveau objet = la référence dans sb change
            Console.WriteLine(sb);
        } // comme sb est une copie, la nouvelle référence est détruite => pas de modification de l'objet passé en paramètre

        static void TestParamValeurObjModifEtat(StringBuilder sb)
        {
            Console.WriteLine(sb);
            sb.Clear();
            sb.Append(" world"); // On modifie l'état de l'objet et pas la référence => l'objet passé en paramètre sera modifié
            Console.WriteLine(sb);
        }
        #endregion

        #region Exercice Echange
        // Écrire une méthode Swap qui prend en paramètre 2 entiers et qui permet d'inverser le contenu des 2 variables passées en paramètre

        static void Swap(ref int v1, ref int v2)
        {
            int tmp = v1;
            v1 = v2;
            v2 = tmp;
        }
        #endregion

        static bool EstPresent(char cFind, params char[] caracteres)
        {
            foreach (char c in caracteres)
            {
                if (c == cFind)
                {
                    return true;
                }
            }
            return false;
        }

        #region Exercice Tableau
        // - Écrire une méthode qui affiche un tableau d’entier

        // - Écrire une méthode qui permet de saisir :
        //   - La taille du tableau
        //   - Les éléments du tableau

        // - Écrire une méthode qui calcule :
        //   - le maximum
        //   - la moyenne

        // - Faire un menu qui permet de lancer ces méthodes
        //      1 - Saisir le tableau
        //      2 - Afficher le tableau
        //      3 - Afficher le maximum et la moyenne
        //      0 - Quitter
        //      Choix =
        static void AfficherMenu()
        {
            Console.WriteLine("1 - Saisir le tableau");
            Console.WriteLine("2 - Afficher le tableau");
            Console.WriteLine("3 - Afficher le maximum et la moyenne");
            Console.WriteLine("0 - Quitter");
        }

        static void menu()
        {
            AfficherMenu();
            int choix;
            int[] tab = null;
            do
            {
                Console.Write("Choix =");
                choix = Convert.ToInt32(Console.ReadLine());
                switch (choix)
                {
                    case 1:
                        tab = SaisirTab();
                        break;
                    case 2:
                        if (tab != null)
                        {
                            AfficherTab(tab);
                        }
                        break;
                    case 3:
                        if (tab != null)
                        {
                            CalculTab(tab, out int max, out double avg);
                            Console.WriteLine($"Maximum={max} Moyenne={avg}");
                        }
                        break;
                    case 0:
                        Console.WriteLine("Au revoir!");
                        break;
                    default:
                        AfficherMenu();
                        Console.WriteLine($"Le choix {choix} n'existe pas");
                        break;
                }
            }
            while (choix != 0);
        }

        static void AfficherTab(int[] tab)
        {
            Console.Write("[ ");
            foreach (var elm in tab)
            {
                Console.Write($"{elm} ");
            }
            Console.WriteLine("]");
        }

        static int[] SaisirTab()
        {
            Console.Write("Nombre d'élément du tableau =");
            int size = int.Parse(Console.ReadLine());
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}] =");
                t[i] = int.Parse(Console.ReadLine());
            }
            return t;
        }

        static void CalculTab(int[] t, out int maximum, out double moyenne)
        {
            maximum = t[0];
            double somme = 0.0;
            foreach (var v in t)
            {
                if (v > maximum)
                {
                    maximum = v;
                }
                somme += v;
            }
            moyenne = somme / t.Length;
        }

        #endregion

        #region Surcharge de méthode
        // Une méthode peut être surchargée => plusieurs méthodes peuvent avoir le même nom, mais leurs signatures doient être différentes
        // La signature d'une méthode correspond aux types et nombre de paramètres
        // Le type de retour ne fait pas partie de la signature
        static int Somme(int a, int b)
        {
            Console.WriteLine("2 entiers");
            return a + b;
        }

        static double Somme(int a, double b)
        {
            Console.WriteLine("1 entier, 1 double");
            return a + b;
        }

        static double Somme(double a, double b)
        {
            Console.WriteLine("2 double");
            return a + b;
        }
        static string Somme(string a, string b)
        {
            Console.WriteLine("2 string");
            return a + b;
        }
        #endregion

        // Récursivité: une méthode qui s'appelle elle-même
        static int Factorial(int n) // factoriel= 1* 2* … n
        {
            if (n <= 1) // condition de sortie
            {
                return 1;
            }
            else
            {
                return Factorial(n - 1) * n;
            }
        }

        // Corps de méthode
        // une méthode qui comprend une seule expression, peut s'écrire avec l'opérateur =>
        static int Multiplier(int v1, int v2) => v1 * v2;

        // Méthode locale
        static void AfficherSquare(double v)
        {
            Console.WriteLine(Square(v));
            // Une méthode locale n'est visible que par la méthode englobante
            double Square(double v1) => v1 * v1;
        }

        static void AfficherSquare2(double v)
        {
            // Capture
            // Une méthode locale peut accéder aux variables locales et aux paramètres de la méthode englobante
            //double w = v;
            Console.WriteLine(Square());
            //  double Square() => w * w
            double Square() => v * v;
        }
    }
}
