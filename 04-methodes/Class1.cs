﻿using System;

namespace _04_methodes
{
    internal class Class1
    {
        // Dans un programme, il ne peut y avoir qu’une seule classe contenant une méthode Main.
        // Dans le cas contraire, il faut compiler avec l’option -main pour préciser le main utilisé
        // ou avec Visual Studio Propriété->Application-> choisir l'objet de démarrage
        static int Main()
        {
            Console.WriteLine("Autre Main");
            Console.ReadKey();
            return 0;
        }
    }
}
