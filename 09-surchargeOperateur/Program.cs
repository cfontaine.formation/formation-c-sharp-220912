﻿using System;

namespace _09_surchargeOperateur
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region point
            Point a = new Point(2, 3);
            Point b = -a;
            Console.WriteLine(b);
            Point c = new Point(1, 1);
            Point d = a + c;
            Console.WriteLine(d);
            a += c;     // quand on surcharge un opérateur binaire, l'opérateur composé associé est aussi automatiquement surchargé
            Console.WriteLine(a);
            Point e = a * 3;
            Console.WriteLine(e);

            Console.WriteLine(a == c);
            Console.WriteLine(a != c);

            #endregion

            #region fraction
            Fraction fa = new Fraction(1, 2);
            Fraction fb = new Fraction(2, 8);
            Console.WriteLine($"{fa.Calculer()}   {fb.Calculer()}");
            Fraction rs1 = fa + fb;
            Console.WriteLine(rs1);

            Fraction rs2 = fa * fb;
            Console.WriteLine(rs2);

            Fraction rs3 = fa * 2;
            Console.WriteLine(rs3);

            Console.WriteLine(fa == fb);
            Console.WriteLine(fa == fb * 2);
            Console.WriteLine(fa != fb);

            #endregion
            Console.ReadKey();
        }
    }
}
