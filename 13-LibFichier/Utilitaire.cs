﻿using System;
using System.Collections.Generic;
using System.IO;

namespace _13_LibFichier
{
    public static class Utilitaire
    {
        public static void InfoLecteur()
        {
            // DriveInfo fournit des informations sur les lecteurs d'une machine
            DriveInfo[] lecteurs = DriveInfo.GetDrives();
            foreach (var drv in lecteurs)
            {
                Console.WriteLine(drv.Name);              // Nom du lecteur
                Console.WriteLine(drv.TotalSize);         // Espace total du lecteur
                Console.WriteLine(drv.TotalFreeSpace);    // Espace disponible sur le lecteur
                Console.WriteLine(drv.DriveType);         // Espace total du lecteur
                Console.WriteLine(drv.DriveFormat);       // Système de fichiers du lecteur NTFS, FAT ...
            }
        }

        public static void InfoDossier()
        {
            //  Teste si le dossier existe
            if (Directory.Exists(@"C:\Dawan\TestIo"))
            {
                // Liste les répertoires contenu dans le chemin
                string[] rep = Directory.GetDirectories(@"C:\Dawan\TestIo");
                foreach (string r in rep)
                {
                    Console.WriteLine(r);
                }

                // Liste les fichiers du répertoire
                string[] fil = Directory.GetFiles(@"C:\Dawan\TestIo");
                foreach (string f in fil)
                {
                    Console.WriteLine(f);
                }
                if (Directory.Exists(@"C:\Dawan\TestIo\Asup"))
                {
                    Directory.Delete(@"C:\Dawan\TestIo\Asup");
                }
            }
            else
            {
                // Création du répertoire
                Directory.CreateDirectory(@"C:\Dawan\TestIo");
            }
        }

        public static void Parcourir(string path)
        {
            if (Directory.Exists(path))
            {
                string[] file = Directory.GetFiles(path);
                foreach (var f in file)
                {
                    Console.WriteLine($"\t{f}");
                }
                String[] reps = Directory.GetDirectories(path);
                foreach (String rep in reps)
                {
                    Console.WriteLine($"Répertoire: {rep}");
                    Console.WriteLine("_________________________");
                    Parcourir(rep);
                }
            }
        }

        public static void InfoFichier()
        {
            // Teste si le fichier
            if (File.Exists(@"C:\Dawan\TestIo\tmp\asup.txt"))
            {
                // Supprime le fichier
                File.Delete(@"C:\Dawan\TestIo\tmp\asup.txt");
            }
        }

        public static void EcrireFichierTexte(string path)
        {
            StreamWriter sw = null;     // StreamWriter Ecrire un fichier texte
            try
            {
                sw = new StreamWriter(path, true);  // append à true => compléte le fichier s'il existe déjà, à false le fichier est écrasé
                for (int i = 0; i < 10; i++)
                {
                    sw.WriteLine("Hello World");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                    sw.Dispose();
                }
            }
        }

        // Using => Équivalent d'un try / finally + Close()
        public static List<string> LireFichierTexte(string path)
        {
            List<string> lst = new List<string>();
            using (StreamReader sr = new StreamReader(path))
            {
                while (!sr.EndOfStream)
                {
                    lst.Add(sr.ReadLine());
                }
            }
            return lst;
        }

        public static void EcrireFichierBinaire(string path)
        {
            using (FileStream fs = new FileStream(path, FileMode.Create))   // FileStream =>  permet de Lire/Ecrire un fichier binaire
            {
                for (byte b = 0; b < 104; b++)
                {
                    fs.WriteByte(b);    // Ecriture d'un octet dans le fichier
                }
            }
        }

        public static void LireFichierBinaire(string path)
        {
            byte[] buf = new byte[10];
            int nb = 1;

            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                while (nb != 0)
                {
                    nb = fs.Read(buf, 0, buf.Length);   // Lecture de 10 octets au maximum dans le fichier, ils sont placés dans le tableau tab à partir de l'indice 0
                    for (int i = 0; i < nb; i++)        // Read => retourne le nombre d'octets lue dans le fichier
                    {
                        Console.Write($"{buf[i]}");
                    }
                    Console.WriteLine();
                }
            }
        }

        // Exercice: Copie d'un fichier binaire
        public static void Copie(string source, string target)
        {
            FileStream fi = null;
            FileStream fo = null;
            try
            {
                // FileStream =>  permet de Lire/Ecrire un fichier binaire
                byte[] t = new byte[100];
                fi = new FileStream(source, FileMode.Open);
                fo = new FileStream(target, FileMode.CreateNew);
                while (true)
                {
                    // Lit 100 octets au maximum dans le fichier (source) et les place dans le tableau à partir de l'indice 0
                    // Read => retourne le nombre d'octets lue dans le fichier
                    int size = fi.Read(t, 0, t.Length);
                    if (size == 0)
                    {
                        break;
                    }
                    // écrit dans le fichier (cible) les octets qui ont été lue
                    fo.Write(t, 0, size);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (fi != null)
                {
                    fi.Close();
                    fi.Dispose();
                }
                if (fo != null)
                {
                    fo.Close();
                    fo.Dispose();
                }
            }
        }

    }

}
