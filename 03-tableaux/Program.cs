﻿using System;

namespace _03_tableaux
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region tableau à une dimension
            // Déclarer un tableau
            double[] tab = new double[5];

            // Valeur d'nitialisation des éléments du tableau
            // - entier -> 0
            // - double , float ou un decimal -> 0.0
            // - char -> '\u0000'
            // - boolean -> false
            //  -type référence  -> null

            // Accèder à un élément du tableau
            Console.WriteLine(tab[1]);
            tab[0] = 12.3;
            Console.WriteLine(tab[0]);

            // Si l'on essaye d'accéder à un élément en dehors du tableau -> IndexOutOfRangeException
            // Console.WriteLine(tab[10]); 

            // Nombre d'élément du tableau
            Console.WriteLine($"Nombre élément={tab.Length}");

            // Parcourir complétement un tableau (avec un for) 
            for (int i = 0; i < tab.Length; i++)
            {
                Console.WriteLine($"tab[{i}]={tab[i]}");
            }

            // Parcourir complétement un tableau (foreach)
            // foreach (double elm in tab) // ou
            foreach (var elm in tab)       // var -> elm est de type double
            {
                Console.WriteLine(elm);   // elm uniquement en lecture
                //elm = 34.5;             // => Erreur ,on ne peut pas modifier elm
            }

            // On peut utiliser une variable entière pour définir la taille du tableau
            int st = 4;
            string[] tabStr = new string[st];
            tabStr[0] = "Hello";
            foreach (var elm in tabStr)
            {
                Console.WriteLine(elm);
            }

            // Déclaration et initialisation
            char[] tabChr = { 'a', 'z', 'e', 'r' };
            foreach (var e in tabChr)
            {
                Console.WriteLine(e);
            }
            #endregion

            // #region exercice Tableau
            #region Exercice tableau
            // 1 - Trouver la valeur maximale et la moyenne d’un tableau de 5 entier: -7, -6, -4, -8, -3
            // 2 - Modifier le programme pour faire la saisie de la taille du tableau et saisir les éléments du tableau

            //  int[] t ={ -7,-6,-4,-8,-3}; //1

            Console.Write("Taille du tableau=");
            int size = int.Parse(Console.ReadLine());
            int[] t = new int[size];
            for (int i = 0; i < size; i++)
            {
                Console.Write($"t[{i}]=");
                t[i] = int.Parse(Console.ReadLine());
            }
            int max = int.MinValue; // t[0]
            double somme = 0.0;
            foreach (var elm in t)
            {
                if (elm > max)
                {
                    max = elm;
                }
                somme += elm;
            }
            double moyenne = somme / t.Length;
            Console.WriteLine($"Maximum={max} Moyennne={moyenne}");
            #endregion
            #region Tableau Multidimensions
            // Déclaration d'un tableau à 2 dimensions
            // Nombre maximum de dimmesion 32
            int[,] tab2d = new int[4, 2];

            // Accès à un élémént d'un tableau à 2 dimensions
            tab2d[2, 0] = 42;
            Console.WriteLine(tab2d[2, 0]);

            // Nombre d'élément du tableau
            Console.WriteLine(tab2d.Length); //8

            // Nombre de dimension du tableau
            Console.WriteLine(tab2d.Rank); //2

            // Nombre de ligne du tableau
            Console.WriteLine(tab2d.GetLength(0));      // 4
            Console.WriteLine(tab2d.GetLongLength(0));  // 4L

            // Nombre de colonne du tableau
            Console.WriteLine(tab2d.GetLength(1));      // 2

            // Parcourir complétement un tableau à 2 dimension => for
            for (int i = 0; i < tab2d.GetLength(0); i++)
            {
                for (int j = 0; j < tab2d.GetLength(1); j++)
                {
                    Console.Write(tab2d[i, j] + "\t");
                }
                Console.WriteLine();
            }
            // Parcourir complétement un tableau à 2 dimension => foreach
            foreach (var elm in tab2d)// avec var la variable e aura le même type que le tableau
            {
                Console.WriteLine(elm);
            }

            // Tableau d'entier à 3 dimmensions
            int[,,] tab3D = new int[3, 2, 4];
            Console.WriteLine(tab3D[0, 0, 0]);
            for (int dim = 0; dim < tab3D.Rank; dim++)
            {
                Console.WriteLine($"dimension {dim} = {tab3D.GetLength(dim)}");
            }

            // Déclaration et initialisation
            string[,] tabStr2d = { { "az", "er", "ty" }, { "wx", "cv", "bn" } };
            for (int i = 0; i < tabStr2d.GetLength(0); i++)
            {
                for (int j = 0; j < tabStr2d.GetLength(1); j++)
                {
                    Console.Write(tabStr2d[i, j] + "\t");
                }
                Console.WriteLine();
            }

            #endregion

            #region Tableau de tableau
            // Déclaration d'un tableau de tableau
            int[][] tabEsc = new int[3][];
            tabEsc[0] = new int[4];
            tabEsc[1] = new int[2];
            tabEsc[2] = new int[3];
            // Accès à un élément
            Console.WriteLine(tabEsc[0][3]);
            tabEsc[1][1] = 42;

            // Nombre de ligne
            Console.WriteLine(tabEsc.Length);


            // Nombre de colonne
            for (int i = 0; i < tabEsc.Length; i++)
            {
                Console.WriteLine(tabEsc[i].Length);
            }

            // Nombre d'élément total du tableau 
            int nbElm = 0;
            for (int i = 0; i < tabEsc.Length; i++)
            {
                nbElm += tabEsc[i].Length;
                Console.WriteLine(tabEsc[i].Length);
            }
            Console.WriteLine($"nombre d'élément={nbElm}");

            //  Parcourir complétement un tableau de tableau => for
            for (int i = 0; i < tabEsc.Length; i++)
            {
                for (int j = 0; j < tabEsc[i].Length; j++)
                {
                    Console.Write($"{tabEsc[i][j]}\t");
                }
                Console.WriteLine();
            }

            //  Parcourir complétement un tableau de tableau => foreach
            foreach (int[] r in tabEsc)
            {
                foreach (int elm in r)
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }

            // Déclarer et initialiser un tableau de tableau
            int[][] tabEsc2 = new int[][]
            {
                new int[]{1,4,6,7,8},
                new int[]{4,6,},
                new int[]{44,61,78},
            };

            foreach (int[] r in tabEsc2)
            {
                foreach (int elm in r)
                {
                    Console.Write($"{elm}\t");
                }
                Console.WriteLine();
            }
            #endregion
            Console.ReadKey();
        }
    }
}
