﻿using _13_LibFichier;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _14_Fichiers
{
    // Pour ajouter au projet la bibliothèque
    // Cliquer droit sur le projet -> Ajouter -> référence -> choisir 13-LibFichier
    internal class Program
    {
        static void Main(string[] args)
        {
            Utilitaire.InfoLecteur();
            Utilitaire.InfoDossier();
            Utilitaire.Parcourir(@"C:\Dawan\TestIo");
            Utilitaire.InfoFichier();
            Utilitaire.EcrireFichierTexte(@"C:\Dawan\TestIo\Test.txt");
            List<string> l=Utilitaire.LireFichierTexte(@"C:\Dawan\TestIo\Test.txt");
            foreach(var r in l)
            {
                Console.WriteLine(r);
            }

            Utilitaire.EcrireFichierBinaire(@"C:\Dawan\TestIo\Test.bin");
            Utilitaire.LireFichierBinaire(@"C:\Dawan\TestIo\Test.bin");
            Console.ReadKey();
        }
    }
}
