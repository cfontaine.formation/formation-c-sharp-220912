﻿
using _15_LibBdd;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;

namespace _16_Adodotnet
{
    internal class Program
    {
        static void Main(string[] args)
        {
            TestBdd();
            TestDao();
            Console.ReadKey();
        }
        static void TestBdd()
        {
            // La chaine de connection contient toutes les informations pour la connection à la base de donnée
            // Server-> adresse du serveur de bdd
            // Port port de serveur de bdd
            // Database -> nom de la base de donnée
            // Uid -> utilisateur de la base de donnée
            // Pwd -> mot de passe de la bdd

            // Chaine de connection SQLServer sur un serveur
            string chaineConnection = "Server=192.168.1.9;Database=formation_ch;Uid=sa;Pwd=Dawan_2022!sql";


            //  Chaine de connection MySQL en local
            // string chaineConnection = "Server=localhost;Port=3306;Database=formation_ch;Uid=root;Pwd=dawan;SslMode=none";

            // Chaine de connection pour se connecter à Sqlserver express en local avec une authentification avec l'utilisateur windows
            // string chaineConnection = @"Data Source=DESKTOP-9H6VFME\SQLEXPRESS;Initial Catalog=formation_ch;Integrated Security= True";

            // Création de la connexion à la base de donnée SqlConnection
            SqlConnection cnx = new SqlConnection(chaineConnection);

            // Ouverture de la connexion à la base de donnée
            cnx.Open();

            string requete = "INSERT INTO contacts(prenom,nom,email,jour_naissance) VALUES (@prenom,@nom,@email,@journaissance)";
            SqlCommand cmd = new SqlCommand(requete, cnx);
            // Remplacement des paramètres dans la requête (@...) par les valeurs
            cmd.Parameters.AddWithValue("@prenom", "john");
            cmd.Parameters.AddWithValue("@nom", "Doe");
            cmd.Parameters.AddWithValue("@email", "jd@dawan.com");
            cmd.Parameters.AddWithValue("@journaissance", new DateTime(1998, 03, 01));
            cmd.ExecuteNonQuery();  // execution de la requete pour INSERT,UPDATE,DELETE

            requete = "SELECT id,prenom,nom,email,jour_naissance FROM Contacts";
            cmd = new SqlCommand(requete, cnx);
            SqlDataReader r = cmd.ExecuteReader();
            while (r.Read())    // Read() permet de passer à la prochaine "ligne" retourne false quand il n'y a plus de resultat
            {
                Console.WriteLine($"{r.GetInt64(0)} {r.GetString(1)}  {r.GetString(2)}  {r.GetString(3)} {r.GetDateTime(4).ToShortDateString()} ");
            }
            // Fermeture de la connection
            cnx.Close();
            cnx.Dispose();

        }
        static void TestDao()
        {
            Contact c = SaisirContact();
            ContactDao dao = new ContactDao();
            // Récupération de la chaine de connection dans le fichier App.config du projet élément<connectionStrings>
            ContactDao.ChaineConnection = ConfigurationManager.ConnectionStrings["chsqlserver"].ConnectionString;
            Console.WriteLine("id={0}", c.Id); // id=0 => l'objet n'a pas été peristé dans la base de donnée
            dao.SaveOrUpdate(c); // persister l'objet dans la bdd
            Console.WriteLine("id={0}", c.Id); // id a été généré par la bdd
            Console.WriteLine("{0}", c);

            long id = c.Id;
            // Affichage de tous les objets contact
            List<Contact> lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }

            // Lire un objet à partir de son id
            Console.WriteLine("\nLire {0}", id);
            Contact cr = dao.FindById(id);
            Console.WriteLine(cr);

            // Modification 
            Console.WriteLine("\nModification {0}", cr.Id);
            cr.Prenom = "Marcel";
            cr.JourNaissance = new DateTime(1987, 8, 11);
            dao.SaveOrUpdate(cr);
            cr = dao.FindById(cr.Id);
            Console.WriteLine(cr);

            // Effacer
            Console.WriteLine("\neffacer {0}", cr.Id);
            dao.Delete(cr);
            lst = dao.FindAll();
            foreach (Contact co in lst)
            {
                Console.WriteLine(co);
            }
        }

        private static Contact SaisirContact()
        {
            Console.Write("Entrer votre prénom: ");
            string prenom = Console.ReadLine();
            Console.Write("Entrer votre nom: ");
            string nom = Console.ReadLine();
            Console.Write("Entrer votre email: ");
            string email = Console.ReadLine();
            Console.Write("Entrer votre date de naissance (YYYY/MM/DD): ");
            DateTime jdn = DateTime.Parse(Console.ReadLine());
            return new Contact(prenom, nom, email, jdn);
        }
    }
}

