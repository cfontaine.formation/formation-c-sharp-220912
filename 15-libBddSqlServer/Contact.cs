﻿using System;

namespace _15_LibBdd
{
    public class Contact : AbstractEntity
    {
        public string Prenom { get; set; }
        public string Nom { get; set; }

        public string Email { get; set; }

        public DateTime JourNaissance { get; set; }

        public Contact(string prenom, string nom, string email, DateTime jourNaissance)
        {
            Prenom = prenom;
            Nom = nom;
            Email = email;
            JourNaissance = jourNaissance;
        }

        public override string ToString()
        {
            return $"{base.ToString()} {Prenom} {Nom} {Email} {JourNaissance}";
        }
    }
}
