﻿using System;

namespace _06_polymorphisme
{
    internal class Program
    {
        static void Main(string[] args)
        {
            #region Polymorphisme
            //Animal a1 = new Animal(5000, 5);   // Impossible la classe est abstraite
            //a1.EmettreSon();


            Chien ch1 = new Chien(3000, 3, "Laika");
            ch1.EmettreSon();

            Animal a2 = new Chien(2000, 7, "Idefix");   // On peut créer une instance de Chien (classe fille) qui aura une référence, une référence Animal (classe mère)
                                                        // L'objet Chien sera vu comme un Animal, on ne pourra pas accèder aux propriétés et aux méthodes propre au chien (Nom,...)
                                                        // Comme la méthode est virtual dans Animal et est rédéfinie dans Chien, c'est la méthode de Chien qui sera appelée
            a2.EmettreSon();

            //if (a2 is Chien)      // test si a2 est de "type" Chien
            //{

            // Pour passer d'une super-classe à une sous-classe, il faut le faire explicitement avec un cast ou avec l'opérateur as
            //    //Chien ch2 = (Chien)a2;
            //    Chien ch2 = a2 as Chien;  // as equivalant à un cast pour un objet
            //    Console.WriteLine(ch2.Nom);    // avec la référence ch2 (de type Chien), on a bien accès à toutes les propriétées de la classe Chien
            //}
            if (a2 is Chien ch2)    // Avec l'opérateur is, on peut tester si a2 est de type Chien et faire la conversion en même temps
            {
                Console.WriteLine(ch2.Nom);
            }

            Animalerie an = new Animalerie();
            an.Ajouter(new Chien(3000, 3, "Laika"));
            an.Ajouter(new Chien(2000, 7, "Idefix"));
            an.Ajouter(new Chat(4000, 4, 7));
            an.Ajouter(new Chat(300, 9, 4));
            an.Ajouter(new Canard(2000, 2));
            an.Ecouter();
            #endregion

            #region Object
            Animal a3 = new Chien(7000, 4, "Rolo");

            // ToString
            // Console.WriteLine(a3.ToString());
            Console.WriteLine(a3);

            // Equals/GetHashCode
            Chien ch3 = new Chien(3000, 3, "Laika");
            Chien ch4 = new Chien(3000, 3, "Laika");
            Chien ch5 = ch3;
            Console.WriteLine(ch3 == ch4); // si l'opérateur == n'est pas redéfinit, on compare l'égalité des références comme se sont 2 objets différents -> false
            Console.WriteLine(ch3 == ch5); // true ch3 et ch5 sont des références vers le même objet

            Console.WriteLine(ch3.Equals(ch4)); // si Equal a été redéfinie -> true
            #endregion
            #region Interface
            // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            IPeutMarcher ipm = new Chat(3000, 5, 6);     // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            ipm.Courir();

            IPeutMarcher[] p = new IPeutMarcher[4]; // On peut utiliser une référence vers une interface pour référencer une classe qui l'implémente
            p[0] = new Chien(3000, 3, "Laika");
            p[1] = new Chien(3000, 3, "Laika");
            p[2] = new Chat(300, 9, 4);
            p[3] = new Canard(2000, 2);

            foreach (var e in p)
            {
                e.Marcher();
            }

            IPeutVoler ipv = new Canard(2000, 2);
            ipv.Decoller();
            #endregion
            Console.ReadKey();
        }
    }
}
