﻿namespace _06_polymorphisme
{
    internal class Animalerie
    {
        public Animal[] places = new Animal[20];

        private int nbPlaceOccuper;

        public void Ajouter(Animal a)
        {
            if (nbPlaceOccuper < places.Length)
            {
                places[nbPlaceOccuper] = a;
                nbPlaceOccuper++;
            }
        }

        public void Ecouter()
        {
            for (int i = 0; i < nbPlaceOccuper; i++)
            {
                places[i].EmettreSon();
            }
        }

    }
}
