﻿using System;

namespace _06_polymorphisme
{
    internal class Canard : Animal, IPeutMarcher, IPeutVoler  // On peut implémenter plusieurs d'interface
    {
        public Canard(double poid, int age) : base(poid, age)
        {
        }

        public override void EmettreSon()
        {
            Console.WriteLine("Coin coin");
        }

        public void Marcher()
        {
            Console.WriteLine(" Le canard marche");
        }

        public void Courir()
        {
            Console.WriteLine(" Le carnard court");
        }

        public void Atterir()
        {
            Console.WriteLine(" Le canard atterit");
        }

        public void Decoller()
        {
            Console.WriteLine(" Le canard décole");
        }
    }
}
