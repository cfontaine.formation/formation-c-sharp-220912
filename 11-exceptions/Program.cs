﻿using System;

namespace _11_exceptions
{
    internal class Program
    {
        static int Main(string[] args)
        {
            int a = 10;
            try
            {
                int[] tab = new int[5];
                int index = int.Parse(Console.ReadLine());  // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                a = 12;
                Console.WriteLine(tab[index]);              // Peut générer une exception IndexOutOfRangeException , si index est > à Length
                int age = int.Parse(Console.ReadLine());     // Peut générer une exception FormatException, si on saisie autre chose qu'un nombre entier
                TraitementEmploye(age);
                Console.WriteLine("Suite du programme");
                //return 1;
            }

            catch (IndexOutOfRangeException e)  // Attrape les exceptions IndexOutOfRangeException
            {
                Console.WriteLine("L'index est en dehors des limites du tableau");
                Console.WriteLine(e.Message);   // Message => permet de récupérer le messsage de l'exception
                Console.WriteLine(e.StackTrace);
            }
            catch (FormatException e)           // Attrape les exceptions FormatException
            {
                Console.WriteLine("Format de saisie incorrect");
            }
            catch (Exception e)                 // Attrape tous les autres exception
            {
                Console.WriteLine("autre exception");
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }

            finally // Le bloc finally est toujours éxécuté
            {
                // finally -> généralement utilisé pour libérer les ressources
                Console.WriteLine("Toujours éxécuter");
                Console.ReadKey();
            }
            Console.WriteLine($"Fin de programme a={a}");
            Console.ReadKey();
            return 0;
        }

        public static void TraitementEmploye(int age)
        {
            Console.WriteLine("Début traitement employé");
            // Traitement partiel de l'exception AgeNegatifException
            try
            {
                TraitementAge(age);
            }
            catch (AgeNegatifException e) when (e.Age < -10)
            {
                // Traitement partiel de l'exception AgeNegatifException
                Console.WriteLine($"TraitementAge Local de l'exception age={e.Age}  -10");
                throw;  // On relance l'exception pour que l'utilisateur de la méthode la traite a son niveau           
            }
            catch (AgeNegatifException e)
            {
                Console.WriteLine($"TraitementAge Local de l'exception age={e.Age}");
                throw new ArgumentException("traitement employé age négatif", e);   // On relance une exception différente
            }                                                                       // e => référence à l'exception qui a provoquer l'exception
            Console.WriteLine("Fin traitement employé");
        }

        public static void TraitementAge(int age)
        {
            Console.WriteLine("Début traitement age");
            if (age < 0)
            {
                throw new AgeNegatifException(age, "Age négatif");  // throw => Lancer un exception
                                                                    //  Elle va remonter la pile d'appel des méthodes jusqu'à ce qu'elle soit traité par un try/catch
                                                                    //  si elle n'est pas traiter, aprés la méthode Main => arrét du programme
            }
            Console.WriteLine("Fin traitement age");
        }

    }


}
