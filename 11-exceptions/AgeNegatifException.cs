﻿using System;

namespace _11_exceptions
{
    // Exception personnalisée => on hérite de la classe Exception ou d'une classe enfant de Exception
    internal class AgeNegatifException : Exception
    {
        public int Age { get; }

        public AgeNegatifException(int age)
        {
            Age = age;
        }

        public AgeNegatifException(int age, string message) : base(message)
        {
            Age = age;
        }

        public AgeNegatifException(int age, string message, Exception innerException) : base(message, innerException)
        {
            Age = age;
        }
    }
}
