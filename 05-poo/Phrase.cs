﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class Phrase
    {
        string[] _mots;

        public Phrase(params string[] mots)
        {
            _mots = mots;
        }

        // indexeur
        public string this[int index]
        {
            get
            {
                return _mots[index];
            }
            set
            {
                _mots[index] = value;
            }
        }

        // On peut déclarer plusieurs indexeurs, chacun avec des paramètres de différents types
        public int this[string mot]
        {
            get
            {
                for(int i=0;i<_mots.Length;i++)
                {
                    if (mot == _mots[i])
                    {
                        return i;
                    }
                }
                throw new IndexOutOfRangeException();
            }
        }

        public void Afficher()
        {
            foreach (var m in _mots)
            {
                Console.Write(m + " ");
            }
            Console.WriteLine();
        }
    }
}
