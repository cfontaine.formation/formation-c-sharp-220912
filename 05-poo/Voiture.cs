﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal /*sealed*/ class Voiture
    {
        // Variables d'instances => Etat
        // On n'a plus besoin de déclarer les variables d'instances _couleur,_compteurKm, _plaqueIma, elles seront générées automatiquement par les propriétées automatique
        private string _marque = "Honda";
        //private string _couleur = default;// default c#7 valeur par défaut string -> null
        //private string _plaqueIma;
        protected int _vitesse;
        //private int _compteurKm = 20;

        // Propriété
        // Propriété auto-implémenté => la variable d'instance est générée par le compilateur
        public string Couleur { get; set; }
        public string PlaqueIma { get; }
        public int CompteurKm { get; protected set; } = 20; // On peut donner un valeur par défaut à une propriétée (littéral, expression ou une fonction)

        public Personne Proprietaire { get; set; }

        // C# => propriété
        public string Marque
        {
            //get
            //{
            //    return _marque;
            //}
            get => _marque; // C# 7.0
        }

        public int Vitesse
        {
            //get
            //{
            //    return _vitesse;
            //}

            get => _vitesse;// C# 7.0

            //set
            //{
            //    if (value > 0)
            //    {
            //        _vitesse = value;
            //    }
            //}
            set => _vitesse = value > 0 ? value : _vitesse; // C# 7.0
        }



        // Variable de classe
        // static int compteurVoiture;  // Remplacer par une propriétée static
        public static int CptVoiture { get; private set; }
        // Constructeur

        public Voiture() // Constructeur par défaut
        {
            Console.WriteLine("Constructeur défaut Voiture");
            CptVoiture++;
        }

        // this() => Chainnage de constructeur : appel du constructeur par défaut
        public Voiture(string marque, string couleur, string plaqueIma) : this()
        {
            Console.WriteLine("Constructeur  Voiture 3 paramètres");
            this._marque = marque;
            Couleur = couleur;
            PlaqueIma = plaqueIma;
        }

        // this(marque, couleur, plaqueIma) => Chainnage de constructeur : appel du constructeur  Voiture(string marque, string couleur, int vitesse)
        public Voiture(string marque, string couleur, string plaqueIma, int compteurKm) : this(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur  Voiture 4 paramètres");
            CompteurKm = compteurKm;
        }

        public Voiture(string marque, string couleur, string plaqueIma, Personne proprietaire) : this(marque, couleur, plaqueIma)
        {
            this.Proprietaire = proprietaire;
        }

        // constructeur static: appelé avant la création de la première instance ou le référencement d’un membre statique
        static Voiture()
        {
            Console.WriteLine("Constructeur Statique");
        }

        // Destructeur
        //~Voiture()
        //{
        //    Console.WriteLine("Destructeur Voiture");
        //}

        // En C++ et Java
        //public string GetMarque()
        //{
        //    return marque;
        //}

        //public void SetMarque(string marque)
        //{
        //    this.marque = marque;
        //}

        // Méthodes d'instances => comportement
        public void Accelerer(int vAcc)
        {
            if (vAcc > 0)
            {
                _vitesse += vAcc;
            }
        }

        public void Freiner(int vFrn)
        {
            if (vFrn > 0)
            {
                if (vFrn < _vitesse)
                {
                    _vitesse -= vFrn;
                }
                else
                {
                    _vitesse = 0;
                }
            }
        }

        public void Arreter()
        {
            _vitesse = 0;
        }

        public bool EstArrete()
        {
            return _vitesse == 0;
        }

        public virtual void Afficher()
        {
            Console.WriteLine($"Voitutre[{_marque},{Couleur},{PlaqueIma}, {_vitesse},{CompteurKm}]");
            Proprietaire?.Afficher();
        }

        // Méthode de classe
        public static void TestMethodeClasse()
        {
            Console.WriteLine("Méthode de classe");
            //Console.WriteLine(vitesse); // Dans une méthode de classe, on n'a pas accès à une variable d'instance
            // Arreter((40);  // ou une méthode d'instance
            Console.WriteLine(CptVoiture);
        }

        public static bool EgalVitesse(Voiture va, Voiture vb)
        {
            return va._vitesse == vb._vitesse; // mais on peut accéder à une variable d'instance par l'intermédiare d'un objet passé en paramètre
        }

    }
}
