﻿using System;

namespace _05_poo
{
    internal class VoiturePrioritaire : Voiture // VoiturePrioritaire hérite de Voiture
    {
        // base => pour appeler le constructeur de la classe mère
        public VoiturePrioritaire() //:base() -> Implicite
        {
            Console.WriteLine("Constructeur par defaut de voiture prioritaire");
        }

        public VoiturePrioritaire(string marque, string couleur, string plaqueIma, bool gyro) : base(marque, couleur, plaqueIma)
        {
            Console.WriteLine("Constructeur 4 paramètres de voiture prioritaire");
            Gyro = gyro;
        }

        public bool Gyro { get; set; }

        public void Alummer()
        {
            Gyro = true;
            Vitesse += 10;
            //_vitesse += 10;
            //CompteurKm = 1000;
        }
        public void Eteindre()
        {
            Gyro = false;
        }

        // Redéfinition
        public override void Afficher()
        {
            base.Afficher();
            Console.WriteLine("Gyro= " + Gyro);
        }

        // Occultation => redéfinir une méthode d'une classe mère et  « casser » le lien vers la classe mère
        public new void Accelerer(int vAcc)
        {
            // base => pour appeler une méthode de la classe mère
            base.Accelerer(vAcc * 2);
        }

    }
}
