﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class CompteBancaire
    {
        public double Solde { get; protected set; } =50.0;
        public string Iban { get; }
        public string Titulaire{ get; private set; }

        private static int nbCompte;

        public CompteBancaire()
        {
            nbCompte++;
            Iban = "fr5962-0000-0000-" + nbCompte;
        }

        public CompteBancaire(string titulaire) :this()
        {
            this.Titulaire = titulaire;
            //this.titulaire = titulaire ?? throw new ArgumentNullException(nameof(titulaire));
        }

        public CompteBancaire(double solde, string titulaire) :this(titulaire)
        {
            this.Solde = solde;
       //     this.titulaire = titulaire;
        }

        public void Crediter(double valeur)
        {
            if (valeur > 0)
            {
                Solde += valeur;
            }
        }

        public void Debiter(double valeur)
        {
            if (valeur > 0)
            {
                Solde -= valeur;
            }
        }
        public bool EstPositif()
        {
            return Solde >= 0.0;
        }

        public void Afficher()
        {
            Console.WriteLine("______________________________");
            Console.WriteLine($"Solde={Solde}");
            Console.WriteLine($"Iban={Iban}");
            Console.WriteLine($"Titulaire={Titulaire}");
            Console.WriteLine("______________________________");
            ;        }
    }
}
