﻿using System.Text;

namespace _05_poo
{
    // Méthodes d'extension pour ajouter des fonctionnalités à des classes ou des structures existantes
    // Elle doit être définie dans une classe static
    internal static class Extension
    {

        // Une méthode d'extension doit être static et le premier paramètre doit être : this typeEtendu nomParametre
        // Ici, on ajoute un méthode Inverser à la classe .Net string
        public static string Inverser(this string s)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = s.Length - 1; i >= 0; i--)
            {
                sb.Append(s[i]);
            }
            return sb.ToString();
        }
    }
}
