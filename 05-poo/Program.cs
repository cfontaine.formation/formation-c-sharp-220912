﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Appel d'une méthode de classe
            Voiture.TestMethodeClasse();

            // Appel d'une variable de classe
            Console.WriteLine($"Nb voiture={Voiture.CptVoiture}");

            // Instantiation de la classe Voiture
            Voiture v1 = new Voiture();
            Console.WriteLine($"Nb voiture={Voiture.CptVoiture}");
  
            // Accès à une variable d'instance
            // v1._vitesse = 20;

            // Accès à une propriété en ecriture (set)
            v1.Vitesse = 20;

            // Accès à une propriété en lecture (get)
            Console.WriteLine(v1.Vitesse); //v1._vitesse

            // Appel d’une méthode d’instance
            v1.Accelerer(15);
            v1.Afficher();
            v1.Freiner(10);
            Console.WriteLine(v1.EstArrete());
            v1.Arreter();
            Console.WriteLine(v1.EstArrete());
            v1.Afficher();

            Voiture v2 = new Voiture();
            Console.WriteLine($"Nb voiture={Voiture.CptVoiture}");
            v2.Afficher();
            v2.Vitesse = 10;//_vitesse = 10;
            Console.WriteLine(v2.Vitesse);


            v2 = null; // En affectant, null à la références v2.Il n'y a plus de référence sur l'objet voiture
                       // Il sera détruit lors de la prochaine execution du garbage collector

            // Forcer l'appel le garbage collector
            // appel explicite du garbage collector (à éviter)
            //GC.Collect();

            Voiture v3 = null;
            Console.WriteLine($"Nb voiture={Voiture.CptVoiture}");
            //v3.Afficher(); // -> Exception
            v3?.Afficher();
            //Console.WriteLine(v3?.marque);
            // Console.WriteLine(v3?.GetMarque());
            Console.WriteLine(v3?.Marque);

            Voiture v4 = new Voiture("Fiat", "Jaune", "fr-1234-ab");
            Console.WriteLine($"Nb voiture={Voiture.CptVoiture}");
            v4.Afficher();

            // Appel de méthode de classe
            Console.WriteLine(Voiture.EgalVitesse(v1, v4));

            // Initialiseur d'objet
            // Avec un initialiseur d'objet on a accès uniquement à ce qui est public

            // Voiture v5 = new Voiture { marque = "Ford", couleur = "Bleu", plaqueIma = "fr-4568-RT", vitesse = 30 };
            // v5.Afficher();

            #region indexeur
            Phrase phr = new Phrase("Il", "pleut", "demain");
            Console.WriteLine(phr[1]);
            phr[2] = "aujourd'hui";
            phr.Afficher();
            Console.WriteLine(phr["Il"]);
            Console.WriteLine(phr["pleut"]);
            // Console.WriteLine(phr["Inconnue"]);
            #endregion

            #region Classe Imbriquée
            Conteneur c = new Conteneur();
            c.Test();
            #endregion

            #region Classe partielle
            Form1 form = new Form1(42);
            form.Afficher();
            #endregion

            #region Classe statique
            //  Math m = new Math();    // ne peut pas être instanciée
            // ne peut pas contenir de constructeurs d’instances
            Console.WriteLine(Math.Abs(-5));    // contient uniquement des membres statiques
            #endregion

            #region  Méthode d'extension
            // ajouter des fonctionnalités à des classes existantes
            string str = "hello world";
            Console.WriteLine(str.Inverser());
            foreach(var chr in str.Reverse())
            {
                Console.WriteLine(chr);
            }
            #endregion

            #region Agrégation
            Personne per1 = new Personne("John", "Doe");
            v4.Proprietaire = per1;
            v4.Afficher();
            #endregion

            #region Héritage
            VoiturePrioritaire vp1 = new VoiturePrioritaire();
            vp1.Accelerer(20);
            vp1.Afficher();
            vp1.Alummer();
            Console.WriteLine(vp1.Gyro);
            #endregion

            #region Redéfinition Occulttion
            VoiturePrioritaire vp2 = new VoiturePrioritaire("Subaru","bleu","az-2222-rt",true);
            vp2.Afficher();
            #endregion

            #region Exercice CompteBancaire
            CompteBancaire cb1 = new CompteBancaire(150.0, "John Doe");
            //cb1.solde = 150.0;
            //cb1.iban = "fr-5962-00000";
            //cb1.titulaire = "John Doe";
            Console.WriteLine(cb1.Solde);


            cb1.Afficher();
            cb1.Crediter(300.0);
            cb1.Debiter(50.0);
            cb1.Afficher();
            Console.WriteLine(cb1.EstPositif());

            CompteBancaire cb2 = new CompteBancaire("Jan Doe");
            cb2.Afficher();

            CompteEpargne ce1 = new CompteEpargne(1.5, "Alan Smithee");
            ce1.Afficher();
            ce1.CalculInteret();
            ce1.Afficher();
            #endregion

            #region Exercice Point
            Point a = new Point(1, 1);
            a.Afficher();
            a.Deplacer(0, 1);
            a.Afficher();
            Point b = new Point(2, 2);
            b.Afficher();
            Console.WriteLine(a.Norme());
            Console.WriteLine(Point.Distance(a,b));
            #endregion

            #region Exercice Agrégation Cercle
            Cercle c1 = new Cercle(new Point(2, 0), 2);
            Cercle cc = new Cercle(new Point(6, 0), 3);
            Cercle cnc = new Cercle(new Point(10, 0), 1);
            Console.WriteLine(c1.Collision(cc));
            Console.WriteLine(c1.Collision(cnc));

            Point p1 = new Point(1, 1);
            Point p10 = new Point(10, 10);
            Console.WriteLine(c1.Contient(p1));
            Console.WriteLine(c1.Contient(p10));
            #endregion

            Console.ReadKey();
        }
    }
}
