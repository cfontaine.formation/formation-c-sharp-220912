﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _05_poo
{
    internal class CompteEpargne : CompteBancaire
    {
        public double Taux { get; set; }

        public CompteEpargne(double taux)
        {
            Taux = taux;
        }
        public CompteEpargne(double taux,string titulaire) :base(titulaire)
        {
            Taux = taux;
        }

        public void CalculInteret()
        {
            Solde*= (1 + Taux / 100);
        }

    }
}
