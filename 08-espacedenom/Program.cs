﻿
using _08_espacedenom.dao;
using _08_espacedenom.gui;

// Definir un Alias
using ConsoleSys = System.Console;

namespace _08_espacedenom
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Nom complet d'une classe namespace.classe
            _08_espacedenom.gui.Window win1 = new _08_espacedenom.gui.Window();
            // avec using _08_espacedenom.gui;
            Window win2 = new Window();

            UserDao dao = new UserDao();

            // En cas de conflit de nom, il faut utiliser le nom complet des classe
            System.Console.WriteLine("Hello");
            _08_espacedenom.gui.Console cnx = new _08_espacedenom.gui.Console();

            // ou un Alias pour une des 2 classes
            Console cnx2 = new Console();
            ConsoleSys.WriteLine("Hello");
        }
    }
}
